export class ProcessedImageData {
  elements: Array<DetectedElement>;
}

export class DetectedElement {
  height: number;
  offsetX: number;
  offsetY: number;
  type: ElementType;
  width: number;
}

export type ElementType = 'App Frame' | 'Image' | 'Button' | 'Menu Button' | 'Input' | 'Anchor';
