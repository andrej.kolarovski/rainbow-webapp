import {DetectedElement} from './result';

// general editing stuff here
export class EditedElement extends DetectedElement {
  id: string;
  opacity?: string;
  border?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  fontColor?: string;
}

export class AppFrameElement extends EditedElement {}

export class MediaElement extends EditedElement {
  source: string;
  clickHandler?: string;
}

export class ImageElement extends MediaElement {
}

export class ClickableElement extends EditedElement {
  clickHandler: string;
}

export class ButtonElement extends ClickableElement {
  placeholder: string;
  buttonType: string;
}

export class MenuButtonElement extends ClickableElement {
}

export class InputElement extends EditedElement {
  placeholder: string;
}

export class AnchorElement extends EditedElement {
  text: string;
  href: string;
  target?: string;
}
