import { Routes } from '@angular/router';

import { StartPageComponent } from './components/start-page/start-page.component';
import { SendTemplateComponent } from './components/send-template/send-template.component';
import { ProcessingResultComponent } from './components/processing-result/processing-result.component';

import * as routes from '../constants/routes';

export const appRoutes: Routes = [
  {path: '', redirectTo: routes.HOME, pathMatch: 'full'},
  {path: routes.HOME, component: StartPageComponent},
  {path: routes.SEND_TEMPLATE, component: SendTemplateComponent},
  {path: routes.RESULT, component: ProcessingResultComponent},
];
