import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAppFrameElementComponent } from './edit-app-frame-element.component';

describe('EditAppFrameElementComponent', () => {
  let component: EditAppFrameElementComponent;
  let fixture: ComponentFixture<EditAppFrameElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAppFrameElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAppFrameElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
