import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppFrameElement } from '../../../types/elements';
import * as constants from '../../../constants/selectValues';

@Component({
  selector: 'app-edit-app-frame-element',
  templateUrl: './edit-app-frame-element.component.html',
  styleUrls: ['./edit-app-frame-element.component.css']
})
export class EditAppFrameElementComponent implements OnInit {
  @Input() element: AppFrameElement;
  @Output() formSubmitted: EventEmitter<void> = new EventEmitter<void>();

  public backgroundColor: string;
  public fontColor: string;
  public fontSize: string;
  public fontWeight: string;
  public borderThickness: string;
  public borderStyle: string;
  public borderColor: string;

  public fontSizeOptions: Array<string>;
  public fontWeightOptions: Array<string>;
  public borderWidthOptions: Array<string>;
  public borderStyleOptions: Array<string>;


  ngOnInit() {
    this.initializeSelectOptions();
    this.initializeFormValues();
  }

  initializeSelectOptions = () => {
    this.fontSizeOptions = constants.fontSizes;
    this.fontWeightOptions = constants.fontWeights;
    this.borderWidthOptions = constants.borderThicknesses;
    this.borderStyleOptions = constants.borderStyles;
  }

  initializeFormValues = () => {
    this.backgroundColor = this.element.background;
    this.fontColor = this.element.fontColor;
    this.fontSize = this.element.fontSize || '16px';
    this.fontWeight = this.element.fontWeight || 'normal';
    this.borderThickness = this.element.border ? this.element.border.split(' ')[0] : '0px';
    this.borderStyle = this.element.border ? this.element.border.split(' ')[1] : 'solid';
    this.borderColor = this.element.border ? this.element.border.split(' ')[2] : null;
  }

  updateAppFrameElement = () => {
    this.element.background = this.backgroundColor;
    this.element.fontColor = this.fontColor;
    this.element.fontSize = this.fontSize;
    this.element.fontWeight = this.fontWeight;
    this.element.border = this.borderThickness + ' ' + this.borderStyle + ' ' + this.borderColor;
    this.formSubmitted.emit();
  }

  backgroundColorSelected = (color: string) => {
    this.backgroundColor = color;
  }

  fontColorSelected = (color: string) => {
    this.fontColor = color;
  }

  fontWeightSelected = (weight: string) => {
    this.fontWeight = weight;
  }

  fontSizeSelected = (size: string) => {
    this.fontSize = size;
  }

  borderThicknessSelected = (thickness: string) => {
    this.borderThickness = thickness;
  }

  borderStyleSelected = (style: string) => {
    this.borderStyle = style;
  }

  borderColorSelected = (color: string) => {
    this.borderColor = color;
  }
}
