import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

const BLACK = '#212529';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class ColorPickerComponent implements OnInit {
  @Input() color: string;
  @Input() title: string;
  @Output() colorSelected: EventEmitter<string> = new EventEmitter<string>();
  public selectedColor: string;

  ngOnInit() {
    this.selectedColor = this.color || BLACK;
  }

  selectColor = (color: string) => {
    this.colorSelected.emit(color);
  }
}
