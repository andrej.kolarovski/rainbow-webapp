import { Component, Output, EventEmitter } from '@angular/core';
import { SampleTemplatesService } from '../../services/sample-templates.service';

import { Base64EncodedImage } from '../../../types/image';

@Component({
  selector: 'app-templates-gallery',
  templateUrl: './templates-gallery.component.html',
  styleUrls: ['./templates-gallery.component.css']
})
export class TemplatesGalleryComponent {
  @Output() templateSelected: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private sampleTemplatesService: SampleTemplatesService
  ) { }

  getTemplates = (): Array<Base64EncodedImage> => {
    return this.sampleTemplatesService.getTemplates();
  }

  onTemplateSelected = (template: string) => {
    this.templateSelected.emit(template);
  }
}
