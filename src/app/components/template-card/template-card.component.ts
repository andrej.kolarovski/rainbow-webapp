import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Base64EncodedImage } from '../../../types/image';

@Component({
  selector: 'app-template-card',
  templateUrl: './template-card.component.html',
  styleUrls: ['./template-card.component.css']
})
export class TemplateCardComponent {
  @Input() template: Base64EncodedImage;
  @Output() templateSelected: EventEmitter<string> = new EventEmitter<string>();

  getImageSource = (): string => {
    return this.template ? this.template.image : 'assets/images/white_bg.png';
  }

  selectTemplate = () => {
    if (!this.template) { return; }
    this.templateSelected.emit(this.template.image);
  }
}
