import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAnchorElementComponent } from './edit-anchor-element.component';

describe('EditAnchorElementComponent', () => {
  let component: EditAnchorElementComponent;
  let fixture: ComponentFixture<EditAnchorElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAnchorElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAnchorElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
