import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AnchorElement } from '../../../types/elements';
import * as constants from '../../../constants/selectValues';

@Component({
  selector: 'app-edit-anchor-element',
  templateUrl: './edit-anchor-element.component.html',
  styleUrls: ['./edit-anchor-element.component.css']
})
export class EditAnchorElementComponent implements OnInit {
  @Input() element: AnchorElement;
  @Output() formSubmitted: EventEmitter<void> = new EventEmitter<void>();

  public anchorText: string;
  public anchorHref: string;
  public openInNewTab: boolean;
  public fontColor: string;
  public fontWeight: string;
  public fontSize: string;

  public fontSizeOptions: Array<string>;
  public fontWeightOptions: Array<string>;

  ngOnInit() {
    this.initializeSelectOptions();
    this.initializeFormValues();
  }

  initializeSelectOptions = () => {
    this.fontSizeOptions = constants.fontSizes;
    this.fontWeightOptions = constants.fontWeights;
  }

  initializeFormValues = () => {
    this.anchorText = this.element.text;
    this.anchorHref = this.element.href;
    this.openInNewTab = this.element.target === '_blank';
    this.fontColor = this.element.fontColor;
    this.fontSize = this.element.fontSize || '16px';
    this.fontWeight = this.element.fontWeight || 'normal';
  }

  updateAnchorElement = () => {
    this.element.text = this.anchorText;
    this.element.href = this.anchorHref;
    this.element.target = this.openInNewTab ? '_blank' : null;
    this.element.fontColor = this.fontColor;
    this.element.fontSize = this.fontSize;
    this.element.fontWeight = this.fontWeight;
    this.formSubmitted.emit();
  }

  fontColorSelected = (color: string) => {
    this.fontColor = color;
  }

  fontSizeSelected = (size: string) => {
    this.fontSize = size;
  }

  fontWeightSelected = (weight: string) => {
    this.fontWeight = weight;
  }
}
