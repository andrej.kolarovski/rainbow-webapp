import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SelectedImageService } from '../../services/selected-image.service';
import { ProcessImageService } from '../../services/process-image.service';
import { ProcessingResultService } from '../../services/processing-result.service';
import { EditResultService } from '../../services/edit-result.service';
import { Base64EncodedImage } from '../../../types/image';
import { ProcessedImageData } from '../../../types/result';

import * as routes from '../../../constants/routes';

@Component({
  selector: 'app-send-template',
  templateUrl: './send-template.component.html',
  styleUrls: ['./send-template.component.css']
})
export class SendTemplateComponent implements OnDestroy, OnInit {
  public processing: boolean;
  private processImageSubscription: Subscription;
  private image: string;

  constructor(
    private selectedImageService: SelectedImageService,
    private processImageService: ProcessImageService,
    private processingResultService: ProcessingResultService,
    private editResultService: EditResultService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.image = this.selectedImageService.getSelectedImage();
  }

  getSelectedTemplate = (): Base64EncodedImage => {
    return { image: this.image };
  }

  processImage = () => {
    if (this.processing) { return; }

    this.processing = true;
    this.processImageSubscription = this.processImageService.processImage(this.image)
    .subscribe(
      this.imageProcessed,
      this.handleError
    );
  }

  imageProcessed = (result: ProcessedImageData) => {
    this.processing = false;
    this.processingResultService.addProcessedImage(result);
    this.editResultService.initializeEditedElements(result.elements);
    this.router.navigate([routes.RESULT]);
  }

  handleError = (error: any) => {
    this.processing = false;
  }

  ngOnDestroy() {
    this.processImageSubscription.unsubscribe();
  }
}
