import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendTemplateComponent } from './send-template.component';

describe('SendTemplateComponent', () => {
  let component: SendTemplateComponent;
  let fixture: ComponentFixture<SendTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
