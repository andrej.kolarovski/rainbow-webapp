import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select-dropdown',
  templateUrl: './select-dropdown.component.html',
  styleUrls: ['./select-dropdown.component.css']
})
export class SelectDropdownComponent implements OnInit {
  @Input() options: Array<string>;
  @Input() defaultSelection: string;
  @Input() currentSelection: string;
  @Output() selected: EventEmitter<string> = new EventEmitter<string>();
  public selection: string;

  ngOnInit() {
    this.selection = this.currentSelection || this.defaultSelection || this.options[0];
  }

  onSelected = () => {
    this.selected.emit(this.selection);
  }
}
