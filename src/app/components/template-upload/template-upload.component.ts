import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Base64EncodeService } from '../../services/base64-encode.service';
import { Base64EncodedImage } from '../../../types/image';

@Component({
  selector: 'app-template-upload',
  templateUrl: './template-upload.component.html',
  styleUrls: ['./template-upload.component.css']
})
export class TemplateUploadComponent {
  @Output() templateSelected: EventEmitter<string> = new EventEmitter<string>();

  public busyEncoding: boolean;
  public template: Base64EncodedImage;

  constructor(
    private base64EncodeService: Base64EncodeService
  ) { }

  addFileClick = () => {
    document.getElementById('fileInput').click();
  }

  onFileInputChange = (event: any) => {
    if (this.busyEncoding) { return; }
    this.busyEncoding = true;
    const file = event.srcElement.files[0];
    this.base64EncodeService.base64Encode(file, this.onFileEncoded);
  }

  onFileEncoded = (b64String: string) => {
    this.busyEncoding = false;
    this.template = new Base64EncodedImage();
    this.template.image = b64String;
  }

  onTemplateSelected = (template: string) => {
    this.templateSelected.emit(template);
  }
}
