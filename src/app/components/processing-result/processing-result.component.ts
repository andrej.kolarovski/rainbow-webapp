import { Component, OnInit } from '@angular/core';
import { ProcessingResultService } from '../../services/processing-result.service';
import { SelectedImageService } from '../../services/selected-image.service';
import { EditResultService } from '../../services/edit-result.service';
import { HtmlGeneratorService } from '../../services/html-generator.service';
import { ProcessedImageData } from '../../../types/result';

@Component({
  selector: 'app-processing-result',
  templateUrl: './processing-result.component.html',
  styleUrls: ['./processing-result.component.css']
})
export class ProcessingResultComponent implements OnInit {
  public resultHtml: string;
  public downloadUrl: string;
  private processingResult: ProcessedImageData;

  constructor(
    private processingResultService: ProcessingResultService,
    private selectedImageService: SelectedImageService,
    private editResultService: EditResultService,
    private htmlGeneratorService: HtmlGeneratorService,
  ) { }

  ngOnInit() {
    this.processingResult = this.processingResultService.getProcessedImage();
    // temp
    this.editResultService.initializeEditedElements(this.processingResult.elements);
    this.generateHtml();
    this.setDownloadUrl();
  }

  getSelectedImage = (): string => {
    return this.selectedImageService.getSelectedImage();
  }

  getEditedElements = () => {
    return this.editResultService.getEditedElements();
  }

  elementUpdated = () => {
    this.generateHtml();
    this.setDownloadUrl();
  }

  generateHtml = () => {
    const elements = this.getEditedElements();
    const isPreview = true;
    this.resultHtml = this.htmlGeneratorService.generateHtml(elements, isPreview);
  }

  setDownloadUrl = () => {
    const elements = this.getEditedElements();
    const isPreview = false;
    const outputHtml = this.htmlGeneratorService.generateHtml(elements, isPreview);

    const blob = new Blob([outputHtml], { type: 'text/plain' });
    this.downloadUrl = window.URL.createObjectURL(blob);
  }
}
