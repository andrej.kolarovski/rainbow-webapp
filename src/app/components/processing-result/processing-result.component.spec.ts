import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessingResultComponent } from './processing-result.component';

describe('ProcessingResultComponent', () => {
  let component: ProcessingResultComponent;
  let fixture: ComponentFixture<ProcessingResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessingResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
