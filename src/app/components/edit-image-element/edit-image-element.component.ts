import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Base64EncodeService } from '../../services/base64-encode.service';
import { ImageElement } from '../../../types/elements';
import * as constants from '../../../constants/selectValues';

@Component({
  selector: 'app-edit-image-element',
  templateUrl: './edit-image-element.component.html',
  styleUrls: ['./edit-image-element.component.css']
})
export class EditImageElementComponent implements OnInit {
  @Input() element: ImageElement;
  @Output() formSubmitted: EventEmitter<void> = new EventEmitter<void>();
  public imageSource: string;
  public imageDataUrl: string;
  public backgroundColor: string;
  public borderThickness: string;
  public borderStyle: string;
  public borderColor: string;

  public borderWidthOptions: Array<string>;
  public borderStyleOptions: Array<string>;

  public imageUploaderId: string;
  public busyEncoding: boolean;

  constructor(
    private base64EncodeService: Base64EncodeService,
  ) { }

  ngOnInit() {
    this.imageUploaderId = 'upload_image_' + this.element.id;
    this.initializeSelectOptions();
    this.initializeFormData();
  }

  initializeSelectOptions = () => {
    this.borderWidthOptions = constants.borderThicknesses;
    this.borderStyleOptions = constants.borderStyles;
  }

  initializeFormData = () => {
    if (this.base64EncodeService.isBase64String(this.element.source)) {
      this.imageDataUrl = this.element.source;
    } else {
      this.imageSource = this.element.source;
    }
    this.backgroundColor = this.element.background;
    this.borderThickness = this.element.border ? this.element.border.split(' ')[0] : '0px';
    this.borderStyle = this.element.border ? this.element.border.split(' ')[1] : 'solid';
    this.borderColor = this.element.border ? this.element.border.split(' ')[2] : null;
  }

  updateImageElement = () => {
    this.element.source = this.imageDataUrl || this.imageSource;
    this.element.background = this.backgroundColor;
    this.element.border = this.borderThickness + ' ' + this.borderStyle + ' ' + this.borderColor;
    this.formSubmitted.emit();
  }

  backgroundColorSelected = (color: string) => {
    this.backgroundColor = color;
  }

  borderThicknessSelected = (thickness: string) => {
    this.borderThickness = thickness;
  }

  borderStyleSelected = (style: string) => {
    this.borderStyle = style;
  }

  borderColorSelected = (color: string) => {
    this.borderColor = color;
  }

  uploadImageClick = () => {
    document.getElementById(this.imageUploaderId).click();
  }

  onImageAdded = (event: any) => {
    if (this.busyEncoding) { return; }
    this.busyEncoding = true;
    const file = event.srcElement.files[0];
    this.base64EncodeService.base64Encode(file, this.onFileEncoded);
  }

  onFileEncoded = (b64String: string) => {
    this.busyEncoding = false;
    this.imageDataUrl = b64String;
  }

  removeUploadedImage = () => {
    this.imageDataUrl = null;
  }
}
