import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditImageElementComponent } from './edit-image-element.component';

describe('EditImageElementComponent', () => {
  let component: EditImageElementComponent;
  let fixture: ComponentFixture<EditImageElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditImageElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditImageElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
