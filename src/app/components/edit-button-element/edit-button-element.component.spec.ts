import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditButtonElementComponent } from './edit-button-element.component';

describe('EditButtonElementComponent', () => {
  let component: EditButtonElementComponent;
  let fixture: ComponentFixture<EditButtonElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditButtonElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditButtonElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
