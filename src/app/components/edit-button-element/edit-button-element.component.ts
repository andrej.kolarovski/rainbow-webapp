import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ButtonElement } from '../../../types/elements';
import * as constants from '../../../constants/selectValues';

@Component({
  selector: 'app-edit-button-element',
  templateUrl: './edit-button-element.component.html',
  styleUrls: ['./edit-button-element.component.css']
})
export class EditButtonElementComponent implements OnInit {
  @Input() element: ButtonElement;
  @Output() formSubmitted: EventEmitter<void> = new EventEmitter<void>();

  public buttonPlaceholder: string;
  public buttonType: string;
  public backgroundColor: string;
  public fontColor: string;
  public fontSize: string;
  public fontWeight: string;
  public borderThickness: string;
  public borderStyle: string;
  public borderColor: string;

  public fontSizeOptions: Array<string>;
  public fontWeightOptions: Array<string>;
  public borderWidthOptions: Array<string>;
  public borderStyleOptions: Array<string>;
  public buttonTypeOptions: Array<string>;

  ngOnInit() {
    this.initializeSelectOptions();
    this.initializeFormValues();
  }

  initializeSelectOptions = () => {
    this.fontSizeOptions = constants.fontSizes;
    this.fontWeightOptions = constants.fontWeights;
    this.borderWidthOptions = constants.borderThicknesses;
    this.borderStyleOptions = constants.borderStyles;
    this.buttonTypeOptions = constants.buttonTypes;
  }

  initializeFormValues = () => {
    this.buttonPlaceholder = this.element.placeholder;
    this.buttonType = this.element.buttonType || 'button';
    this.backgroundColor = this.element.background;
    this.fontColor = this.element.fontColor;
    this.fontSize = this.element.fontSize || '16px';
    this.fontWeight = this.element.fontWeight || 'normal';
    this.borderThickness = this.element.border ? this.element.border.split(' ')[0] : '0px';
    this.borderStyle = this.element.border ? this.element.border.split(' ')[1] : 'solid';
    this.borderColor = this.element.border ? this.element.border.split(' ')[2] : null;
  }

  updateButtonElement = () => {
    this.element.placeholder = this.buttonPlaceholder;
    this.element.buttonType = this.buttonType;
    this.element.background = this.backgroundColor;
    this.element.fontColor = this.fontColor;
    this.element.fontSize = this.fontSize;
    this.element.fontWeight = this.fontWeight;
    this.element.border = this.borderThickness + ' ' + this.borderStyle + ' ' + this.borderColor;
    this.formSubmitted.emit();
  }

  buttonTypeSelected = (type: string) => {
    this.buttonType = type;
  }

  backgroundColorSelected = (color: string) => {
    this.backgroundColor = color;
  }

  fontColorSelected = (color: string) => {
    this.fontColor = color;
  }

  fontWeightSelected = (weight: string) => {
    this.fontWeight = weight;
  }

  fontSizeSelected = (size: string) => {
    this.fontSize = size;
  }

  borderThicknessSelected = (thickness: string) => {
    this.borderThickness = thickness;
  }

  borderStyleSelected = (style: string) => {
    this.borderStyle = style;
  }

  borderColorSelected = (color: string) => {
    this.borderColor = color;
  }
}
