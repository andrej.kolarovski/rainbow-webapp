import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SelectedImageService } from '../../services/selected-image.service';

import * as routes from '../../../constants/routes';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent {

  constructor(
    private selectedImageService: SelectedImageService,
    private router: Router,
  ) { }

  onTemplateSelected = (template: string) => {
    this.selectedImageService.setSelectedImage(template);
    this.router.navigate([routes.SEND_TEMPLATE]);
  }
}
