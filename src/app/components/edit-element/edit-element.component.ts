import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EditedElement } from '../../../types/elements';

@Component({
  selector: 'app-edit-element',
  templateUrl: './edit-element.component.html',
  styleUrls: ['./edit-element.component.css']
})
export class EditElementComponent implements OnInit {
  @Input() element: EditedElement;
  @Output() elementUpdated: EventEmitter<void> = new EventEmitter<void>();

  public editing: boolean;
  private resultImage: HTMLElement;

  constructor() { }

  ngOnInit() {
    this.resultImage = document.getElementById('resultImage');
  }

  getStyle = () => {
    return {
      'width': this.element.width,
      'height': this.element.height,
      'top': this.element.offsetY,
      'left': this.element.offsetX
    };
  }

  toggleEdit = () => {
    this.editing = !this.editing;
  }

  editElementFinished = () => {
    this.editing = false;
    this.elementUpdated.emit();
  }
}
