import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMenuButtonElementComponent } from './edit-menu-button-element.component';

describe('EditMenuButtonElementComponent', () => {
  let component: EditMenuButtonElementComponent;
  let fixture: ComponentFixture<EditMenuButtonElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMenuButtonElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMenuButtonElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
