import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuButtonElement } from '../../../types/elements';
import * as constants from '../../../constants/selectValues';

@Component({
  selector: 'app-edit-menu-button-element',
  templateUrl: './edit-menu-button-element.component.html',
  styleUrls: ['./edit-menu-button-element.component.css']
})
export class EditMenuButtonElementComponent implements OnInit {
  @Input() element: MenuButtonElement;
  @Output() formSubmitted: EventEmitter<void> = new EventEmitter<void>();

  public backgroundColor: string;
  public borderThickness: string;
  public borderStyle: string;
  public borderColor: string;

  public borderWidthOptions: Array<string>;
  public borderStyleOptions: Array<string>;

  ngOnInit() {
    this.initializeSelectOptions();
    this.initializeFormData();
  }

  initializeSelectOptions = () => {
    this.borderWidthOptions = constants.borderThicknesses;
    this.borderStyleOptions = constants.borderStyles;
  }

  initializeFormData = () => {
    this.backgroundColor = this.element.background;
    this.borderThickness = this.element.border ? this.element.border.split(' ')[0] : '0px';
    this.borderStyle = this.element.border ? this.element.border.split(' ')[1] : 'solid';
    this.borderColor = this.element.border ? this.element.border.split(' ')[2] : null;
  }

  updateMenuButtonElement = () => {
    this.element.background = this.backgroundColor;
    this.element.border = this.borderThickness + ' ' + this.borderStyle + ' ' + this.borderColor;
    this.formSubmitted.emit();
  }

  backgroundColorSelected = (color: string) => {
    this.backgroundColor = color;
  }

  borderThicknessSelected = (thickness: string) => {
    this.borderThickness = thickness;
  }

  borderStyleSelected = (style: string) => {
    this.borderStyle = style;
  }

  borderColorSelected = (color: string) => {
    this.borderColor = color;
  }
}
