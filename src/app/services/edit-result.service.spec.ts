import { TestBed, inject } from '@angular/core/testing';

import { EditResultService } from './edit-result.service';

describe('EditResultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditResultService]
    });
  });

  it('should be created', inject([EditResultService], (service: EditResultService) => {
    expect(service).toBeTruthy();
  }));
});
