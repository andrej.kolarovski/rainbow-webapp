import { Injectable } from '@angular/core';

@Injectable()
export class Base64EncodeService {

  base64Encode = (file: File, callback: Function) => {
    const reader = new FileReader();
    reader.onload = () => callback(reader.result);
    reader.readAsDataURL(file);
  }

  isBase64String = (string: string): boolean => {
    return string.indexOf('data:image') > -1;
  }
}
