import { TestBed, inject } from '@angular/core/testing';

import { JsGeneratorService } from './js-generator.service';

describe('JsGeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JsGeneratorService]
    });
  });

  it('should be created', inject([JsGeneratorService], (service: JsGeneratorService) => {
    expect(service).toBeTruthy();
  }));
});
