import { Injectable } from '@angular/core';
import { DetectedElement } from '../../types/result';
import {
  ImageElement,
  ButtonElement,
  MenuButtonElement,
  InputElement,
  AppFrameElement,
  AnchorElement,
  EditedElement,
} from '../../types/elements';

@Injectable()
export class EditResultService {
  private editedElements: Array<EditedElement> = [];

  initializeEditedElements = (detectedElements: Array<DetectedElement>) => {
    detectedElements.forEach((element: DetectedElement, index: number) => {
      switch (element.type) {
        case 'Button': return this.addNewButtonElement(element, 'btn_' + index.toString());
        case 'Menu Button': return this.addNewMenuButtonElement(element, 'menu_btn_' + index.toString());
        case 'Image': return this.addNewImageElement(element, 'img_' + index.toString());
        case 'Input': return this.addNewInputElement(element, 'input_' + index.toString());
        case 'Anchor': return this.addNewAnchorElement(element, 'anchor_' + index.toString());
        case 'App Frame': return this.addNewAppFrameElement(element, 'root');
        default: return;
      }
    });
  }

  addNewImageElement = (element: DetectedElement, id: string) => {
    let imageElement = new ImageElement();
    imageElement = { ...element, source: '', id: id };
    this.editedElements.push(imageElement);
  }

  addNewButtonElement = (element: DetectedElement, id: string) => {
    let buttonElement = new ButtonElement();
    buttonElement = { ...element, buttonType: '', placeholder: '', clickHandler: '', id: id };
    this.editedElements.push(buttonElement);
  }

  addNewMenuButtonElement = (element: DetectedElement, id: string) => {
    let menuButtonElement = new MenuButtonElement();
    menuButtonElement = { ...element, clickHandler: '', id: id };
    this.editedElements.push(menuButtonElement);
  }

  addNewInputElement = (element: DetectedElement, id: string) => {
    let inputElement = new InputElement();
    inputElement = { ...element, placeholder: '', id: id };
    this.editedElements.push(inputElement);
  }

  addNewAnchorElement = (element: DetectedElement, id: string) => {
    let anchorElement = new AnchorElement();
    anchorElement = { ...element, text: '', href: '', id: id };
    this.editedElements.push(anchorElement);
  }

  addNewAppFrameElement = (element: DetectedElement, id: string) => {
    let appFrameElement = new AppFrameElement();
    appFrameElement = { ...element, id: id };
    this.editedElements.push(appFrameElement);
  }

  getEditedElements = (): Array<EditedElement> => {
    return this.editedElements;
  }
}
