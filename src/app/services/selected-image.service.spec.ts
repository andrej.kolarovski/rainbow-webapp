import { TestBed, inject } from '@angular/core/testing';

import { SelectedImageService } from './selected-image.service';

describe('SelectedImageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectedImageService]
    });
  });

  it('should be created', inject([SelectedImageService], (service: SelectedImageService) => {
    expect(service).toBeTruthy();
  }));
});
