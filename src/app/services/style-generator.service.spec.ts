import { TestBed, inject } from '@angular/core/testing';

import { StyleGeneratorService } from './style-generator.service';

describe('StyleGeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StyleGeneratorService]
    });
  });

  it('should be created', inject([StyleGeneratorService], (service: StyleGeneratorService) => {
    expect(service).toBeTruthy();
  }));
});
