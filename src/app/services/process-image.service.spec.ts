import { TestBed, inject } from '@angular/core/testing';

import { ProcessImageService } from './process-image.service';

describe('ProcessImageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcessImageService]
    });
  });

  it('should be created', inject([ProcessImageService], (service: ProcessImageService) => {
    expect(service).toBeTruthy();
  }));
});
