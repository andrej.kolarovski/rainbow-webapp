import { TestBed, inject } from '@angular/core/testing';

import { ProcessingResultService } from './processing-result.service';

describe('ProcessingResultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcessingResultService]
    });
  });

  it('should be created', inject([ProcessingResultService], (service: ProcessingResultService) => {
    expect(service).toBeTruthy();
  }));
});
