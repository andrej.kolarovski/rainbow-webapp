import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as endpoints from '../../constants/endpoints';

@Injectable()
export class ProcessImageService {

  constructor(
    private http: HttpClient
  ) { }

  processImage = (base64String: string) => {
    const url = environment.baseUrl + endpoints.PROCESS_IMAGE;
    const payload = {file: base64String};
    return this.http.post(url, payload);
  }
}
