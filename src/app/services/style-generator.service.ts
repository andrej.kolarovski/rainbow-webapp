import { Injectable } from '@angular/core';
import {
  EditedElement,
} from '../../types/elements';


@Injectable()
export class StyleGeneratorService {
  generatePosition = (
    screenWidth: number,
    screenHeight: number,
    element: EditedElement,
  ): string => {
    let ret = '';

    const horizontalResizeFactor = screenWidth / 100;
    const verticalResizeFactor = screenHeight / 100;

    const widthPercent = Math.floor(element.width / horizontalResizeFactor);
    const heightPercent = Math.floor(element.height / verticalResizeFactor);
    const offsetTop = Math.floor(element.offsetY / verticalResizeFactor);
    const offsetLeft = Math.floor(element.offsetX / horizontalResizeFactor);

    ret += 'position: absolute; ';
    ret += 'width: ' + widthPercent.toString() + '%; ';
    ret += 'height: ' + heightPercent.toString() + '%; ';
    ret += 'top: ' + offsetTop.toString() + '%; ';
    ret += 'left: ' + offsetLeft.toString() + '%; ';

    return ret;
  }

  generateBodyStyle = (screenWidth: number, screenHeight: number): string => {
    const ratio = Math.floor((screenHeight / screenWidth) * 100);
    return 'height: ' + ratio.toString() + 'vw; overflow-x: hidden; position: relative; margin: 0; ';
  }

  generatePreviewDivStyle = (screenWidth: number, screenHeight: number): string => {
    let ret = '';

    ret += 'width: ' + screenWidth.toString() + 'px; ';
    ret += 'height: ' + screenHeight.toString() + 'px; ';
    ret += 'overflow: hidden; position: relative; background-color: #fff; border: 2px solid #c4b1da; ';

    return ret;
  }

  generateRootStyle = (screenWidth: number, screenHeight: number, isPreview: boolean): string => {
    return isPreview ?
      this.generatePreviewDivStyle(screenWidth, screenHeight) :
      this.generateBodyStyle(screenWidth, screenHeight);
  }

  generateStyle = (
    screenWidth: number,
    screenHeight: number,
    element: EditedElement,
    isPreview?: boolean,
  ): string => {
    const isAppFrame = element.type === 'App Frame';
    const isButton = element.type === 'Button';
    const isInput = element.type === 'Input';

    let ret = 'style="';

    ret += isAppFrame ?
      this.generateRootStyle(screenWidth, screenHeight, isPreview) :
      this.generatePosition(screenWidth, screenHeight, element);

    if (isButton) {
      ret += 'cursor: pointer; ';
    }
    if (isInput) {
      ret += 'padding: 5px 10px; ';
    }
    if (element.opacity) {
      ret += 'opacity: ' + element.opacity + '; ';
    }
    if (element.border) {
      ret += 'border: ' + element.border + '; ';
    }
    if (element.background) {
      ret += 'background: ' + element.background + '; ';
    }
    if (element.fontSize) {
      ret += 'font-size: ' + element.fontSize + '; ';
    }
    if (element.fontWeight) {
      ret += ' font-weight: ' + element.fontWeight + '; ';
    }
    if (element.fontColor) {
      ret += ' color: ' + element.fontColor + '; ';
    }

    ret += '" ';
    return ret;
  }
}
