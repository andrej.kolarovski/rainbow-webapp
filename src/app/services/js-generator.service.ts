import { Injectable } from '@angular/core';
import {
  ButtonElement,
  EditedElement,
} from '../../types/elements';

@Injectable()
export class JsGeneratorService {
  generateButtonClickHandler = (button: ButtonElement): string => {
    return button.clickHandler = 'alert(\'Button ' + button.id + ' clicked!\');';
  }

  addButtonClickHandlerToScript = (button: ButtonElement): string => {
    if (!button.clickHandler) {
      this.generateButtonClickHandler(button);
    }
    return button.id + '_click = () => { ' + button.clickHandler + ' }\n';
  }

  generateOnclickAttribute = (button: ButtonElement, isPreview: boolean) => {
    if (!button.clickHandler) {
      this.generateButtonClickHandler(button);
    }
    return 'onclick="' +
          (isPreview ? button.clickHandler : button.id + '_click()') +
          '"';
  }

  generateJs = (elements: Array<EditedElement>): string => {
    let ret = '<script type="text/javascript">';

    elements.forEach((element: EditedElement) => {
      switch (element.type) {
        case 'Button': return ret += this.addButtonClickHandlerToScript(<ButtonElement>element);
        default: return;
      }
    });

    ret += '</script>';
    return ret;
  }
}
