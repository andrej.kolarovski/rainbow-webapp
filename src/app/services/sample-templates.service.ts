import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as endpoints from '../../constants/endpoints';

import {
  Base64EncodedImage,
} from '../../types/image';

@Injectable()
export class SampleTemplatesService {
  public loading: boolean;
  private templates: Array<Base64EncodedImage>;

  constructor(
    private http: HttpClient,
  ) {
    this.loadTemplates();
  }

  loadTemplates = () => {
    this.loading = true;
    const url = environment.baseUrl + endpoints.GET_TEMPLATES;
    this.http.get(url)
    .subscribe(
      this.templatesLoaded,
      this.templatesLoadingError
    );
  }

  templatesLoaded = (data: Array<Base64EncodedImage>) => {
    this.templates = data;
    this.loading = false;
  }

  templatesLoadingError = (error: any) => {
    this.loading = false;
  }

  getTemplates = (): Array<Base64EncodedImage> => {
    return this.templates;
  }
}
