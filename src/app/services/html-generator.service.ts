import { Injectable } from '@angular/core';
import {
  ImageElement,
  ButtonElement,
  MenuButtonElement,
  InputElement,
  AnchorElement,
  AppFrameElement,
  EditedElement,
} from '../../types/elements';
import {
  StyleGeneratorService
} from './style-generator.service';
import {
  JsGeneratorService
} from './js-generator.service';

@Injectable()
export class HtmlGeneratorService {
  constructor(
    private styleGeneratorService: StyleGeneratorService,
    private jsGeneratorService: JsGeneratorService,
  ) { }

  generateImageElementHTML = (element: ImageElement, style: string): string => {
    let ret = '<img ';

    ret += 'id="' + element.id + '" ';
    ret += 'src="' + element.source + '" ';
    ret += style;

    ret += ' />';

    return ret;
  }

  generateButtonElementHTML = (element: ButtonElement, style: string, isPreview: boolean): string => {
    let ret = '<button ';
    const clickHandler = this.jsGeneratorService.generateOnclickAttribute(element, isPreview);

    ret += 'id="' + element.id + '" ';
    ret += style;
    if (element.type) {
      ret += 'type="' + element.buttonType + '" ';
    }
    ret += clickHandler;
    ret += '>';
    ret += element.placeholder || 'Button';
    ret += '</button>';

    return ret;
  }

  generateMenuButtonElementHTML = (element: MenuButtonElement, style: string): string => {
    const menuLine = '<span style="display: block; margin-top: 10%; width: 100%; border-top: 2px solid; border-color: inherit"></span>';
    let ret = '<button ';

    ret += 'id="' + element.id + '" ';
    ret += style;
    ret += '>';
    ret += menuLine + menuLine + menuLine;
    ret += '</button>';

    return ret;
  }

  generateInputElementHTML = (element: InputElement, style: string): string => {
    let ret = '<input ';

    ret += 'id="' + element.id + '" ';
    if (element.placeholder) {
      ret += 'placeholder="' + element.placeholder + '"';
    }
    ret += style;
    ret += '/>';

    return ret;
  }

  generateAnchorElementHTML = (element: AnchorElement, style: string): string => {
    let ret = '<a ';

    ret += 'id="' + element.id + '" ';
    ret += 'href="' + element.href + '" ';
    ret += style;
    if (element.target) {
      ret += 'target="' + element.target + '" ';
    }
    ret += '>' + element.text + '</a>';

    return ret;
  }

  // returns array so it's possible to insert other elements in between
  generateRootElementHTML = (element: AppFrameElement, style: string, isPreview?: boolean): Array<string> => {
    let openingTag = isPreview ?
      '<div ' :
      '<html><body ';

    openingTag += 'id="' + element.id + '" ';
    openingTag += style;
    openingTag += '>';

    const closingTag = isPreview ?
      '</div>' :
      '</body></html>';
    return [openingTag, closingTag];
  }

  getAppFrame = (elements: Array<EditedElement>): EditedElement => {
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].type === 'App Frame') {
        return elements[i];
      }
    }
  }

  generateHtml = (elements: Array<EditedElement>, isPreview?: boolean): string => {
    const appFrame = this.getAppFrame(elements);
    const screenWidth = appFrame.width;
    const screenHeight = appFrame.height;

    let rootElementTags;

    let ret = '';

    if (!isPreview) {
      ret += this.jsGeneratorService.generateJs(elements);
    }

    elements.forEach((element: EditedElement) => {
      const style = this.styleGeneratorService.generateStyle(screenWidth, screenHeight, element, isPreview);
      switch (element.type) {
        case 'Image': return ret += this.generateImageElementHTML(<ImageElement>element, style);
        case 'Button': return ret += this.generateButtonElementHTML(<ButtonElement>element, style, isPreview);
        case 'Menu Button': return ret += this.generateMenuButtonElementHTML(<MenuButtonElement>element, style);
        case 'Input': return ret += this.generateInputElementHTML(<InputElement>element, style);
        case 'Anchor': return ret += this.generateAnchorElementHTML(<AnchorElement>element, style);
        case 'App Frame': return rootElementTags = this.generateRootElementHTML(<AppFrameElement>element, style, isPreview);
        default: return;
      }
    });

    return rootElementTags[0] + ret + rootElementTags[1];
  }
}

