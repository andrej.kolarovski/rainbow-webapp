import { Injectable } from '@angular/core';
import { ProcessedImageData } from '../../types/result';

const mockResult = {
  'elements': [
    {
      'type': 'App Frame',
      'offsetX': 0,
      'offsetY': 0,
      'width': 638,
      'height': 640
    },
    {
      'type': 'Button',
      'offsetX': 376,
      'offsetY': 287,
      'width': 193,
      'height': 34
    },
    {
      'type': 'Button',
      'offsetX': 73,
      'offsetY': 284,
      'width': 193,
      'height': 35
    },
    {
      'type': 'Image',
      'offsetX': 372,
      'offsetY': 62,
      'width': 200,
      'height': 190
    },
    {
      'type': 'Image',
      'offsetX': 70,
      'offsetY': 62,
      'width': 200,
      'height': 189
    },
    {
      'type': 'Menu Button',
      'offsetX': 8,
      'offsetY': 10,
      'width': 46,
      'height': 45
    }
  ]
};

@Injectable()
export class ProcessingResultService {
  private processedImage: any;

  mapIdsToClassNames = (image: any) => {
    image.elements.forEach((element: any) => {
      switch (element.type) {
        case '1': return element.type = 'App Frame';
        case '2': return element.type = 'Anchor';
        case '3': return element.type = 'Button';
        case '4': return element.type = 'Image';
        case '5': return element.type = 'Input';
        case '6': return element.type = 'Menu Button';
      }
    });
    return image;
  }

  addProcessedImage = (image: ProcessedImageData) => this.processedImage = this.mapIdsToClassNames(image);

  getProcessedImage = (): ProcessedImageData => this.processedImage || mockResult;
}
