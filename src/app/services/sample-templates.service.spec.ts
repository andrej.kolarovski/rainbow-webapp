import { TestBed, inject } from '@angular/core/testing';

import { SampleTemplatesService } from './sample-templates.service';

describe('SampleTemplatesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SampleTemplatesService]
    });
  });

  it('should be created', inject([SampleTemplatesService], (service: SampleTemplatesService) => {
    expect(service).toBeTruthy();
  }));
});
