import { TestBed, inject } from '@angular/core/testing';

import { Base64EncodeService } from './base64-encode.service';

describe('Base64EncodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Base64EncodeService]
    });
  });

  it('should be created', inject([Base64EncodeService], (service: Base64EncodeService) => {
    expect(service).toBeTruthy();
  }));
});
