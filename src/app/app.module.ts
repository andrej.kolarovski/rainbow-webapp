import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ColorPickerModule } from 'ngx-color-picker';

import { appRoutes } from './app.routes';
import { AppComponent } from './app.component';

import { ProcessImageService } from './services/process-image.service';
import { SampleTemplatesService } from './services/sample-templates.service';
import { SelectedImageService } from './services/selected-image.service';
import { Base64EncodeService } from './services/base64-encode.service';
import { ProcessingResultService } from './services/processing-result.service';
import { EditResultService } from './services/edit-result.service';
import { HtmlGeneratorService } from './services/html-generator.service';
import { StyleGeneratorService } from './services/style-generator.service';
import { JsGeneratorService } from './services/js-generator.service';

import { HeaderComponent } from './components/header/header.component';
import { TemplatesGalleryComponent } from './components/templates-gallery/templates-gallery.component';
import { TemplateUploadComponent } from './components/template-upload/template-upload.component';
import { StartPageComponent } from './components/start-page/start-page.component';
import { TemplateCardComponent } from './components/template-card/template-card.component';
import { SendTemplateComponent } from './components/send-template/send-template.component';
import { ProcessingResultComponent } from './components/processing-result/processing-result.component';
import { EditImageElementComponent } from './components/edit-image-element/edit-image-element.component';
import { EditElementComponent } from './components/edit-element/edit-element.component';
import { EditButtonElementComponent } from './components/edit-button-element/edit-button-element.component';
import { EditMenuButtonElementComponent } from './components/edit-menu-button-element/edit-menu-button-element.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { PreviewComponent } from './components/preview/preview.component';
import { NoSanitizePipe } from './pipes/no-sanitize.pipe';
import { NoSanitizeHrefPipe } from './pipes/no-sanitize-href.pipe';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { EditAppFrameElementComponent } from './components/edit-app-frame-element/edit-app-frame-element.component';
import { EditInputElementComponent } from './components/edit-input-element/edit-input-element.component';
import { EditAnchorElementComponent } from './components/edit-anchor-element/edit-anchor-element.component';
import { SelectDropdownComponent } from './components/select-dropdown/select-dropdown.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TemplatesGalleryComponent,
    TemplateUploadComponent,
    StartPageComponent,
    TemplateCardComponent,
    SendTemplateComponent,
    ProcessingResultComponent,
    EditImageElementComponent,
    EditElementComponent,
    EditButtonElementComponent,
    EditMenuButtonElementComponent,
    DropdownComponent,
    PreviewComponent,
    NoSanitizePipe,
    NoSanitizeHrefPipe,
    ColorPickerComponent,
    EditAppFrameElementComponent,
    EditInputElementComponent,
    EditAnchorElementComponent,
    SelectDropdownComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    ColorPickerModule,
  ],
  providers: [
    ProcessImageService,
    SampleTemplatesService,
    SelectedImageService,
    Base64EncodeService,
    ProcessingResultService,
    EditResultService,
    HtmlGeneratorService,
    StyleGeneratorService,
    JsGeneratorService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
