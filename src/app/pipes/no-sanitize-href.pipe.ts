import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'noSanitizeHref' })
export class NoSanitizeHrefPipe implements PipeTransform {
   constructor(private sanitizer: DomSanitizer) {

   }
   transform(url: string) {
      return this.sanitizer.bypassSecurityTrustUrl(url);
   }
}
