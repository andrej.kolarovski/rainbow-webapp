export const fontSizes = [
  '1px',
  '2px',
  '3px',
  '4px',
  '5px',
  '6px',
  '7px',
  '8px',
  '9px',
  '10px',
  '11px',
  '12px',
  '13px',
  '14px',
  '15px',
  '16px',
  '18px',
  '20px',
  '22px',
  '24px',
  '26px',
  '28px',
  '30px',
  '36px',
  '42px',
  '48px',
  '54px',
  '60px',
  '72px',
];

export const fontWeights = [
  'normal',
  'bold',
];

export const buttonTypes = [
  'button',
  'submit',
];

export const borderStyles = [
  'solid',
  'dashed',
];

export const borderThicknesses = [
  '0px',
  '1px',
  '2px',
  '3px',
  '4px',
  '5px',
];
